[![GitPitch](https://gitpitch.com/assets/badge.svg)](https://gitpitch.com/gitpitch/kitchen-sink/master?grs=gitlab)

#### WEBSITE: [www.gitpitch.com](https://gitpitch.com) | HOW-TO : [GitPitch Wiki](https://github.com/gitpitch/gitpitch/wiki) | TWITTER: [@gitpitch](https://twitter.com/gitpitch)

# GitPitch - Kitchen Sink
A slideshow presentation that provides a quick GitPitch feature tour.

View the `raw` **PITCHME.md** markdown to see how each slide in the presentation has been implemented.
